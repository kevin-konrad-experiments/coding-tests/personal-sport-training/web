import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingCreateFormComponent } from './training-create-form.component';

describe('TrainingCreateFormComponent', () => {
  let component: TrainingCreateFormComponent;
  let fixture: ComponentFixture<TrainingCreateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingCreateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingCreateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
