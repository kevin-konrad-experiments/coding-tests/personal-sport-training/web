import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {select, Store} from '@ngrx/store';
import * as fromSports from '../../_core/store/sports/sports.reducer';
import * as fromTrainings from '../../_core/store/trainings/trainings.reducer';
import {Observable} from 'rxjs';
import Sport from '../../_core/models/sport.model';
import {selectSportsAll} from '../../_core/store/sports/sports.selectors';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {createTraining} from '../../_core/store/trainings/trainings.actions';
import {selectTrainingsLoading} from '../../_core/store/trainings/trainings.selectors';
import {TrainingCreateBody} from '../../_core/models/training.model';

@Component({
  selector: 'app-training-create-form',
  templateUrl: './training-create-form.component.html',
  styleUrls: ['./training-create-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingCreateFormComponent implements OnInit {
  private trainingForm: FormGroup;
  private currentDate: Date = new Date();
  private sports$: Observable<Sport[]>;
  private loading$: Observable<boolean>;
  private data: TrainingCreateBody = {
    sport_id: undefined,
    name: undefined,
    activities_per_day: 1,
    weekends_only: false,
    period_start: new Date(Date.UTC(this.currentDate.getUTCFullYear(), this.currentDate.getUTCMonth(), this.currentDate.getUTCDate())),
    period_end: undefined,
  };

  constructor(
    private dialogRef: MatDialogRef<TrainingCreateFormComponent>,
    private formBuilder: FormBuilder,
    private sportsStore: Store<fromSports.State>,
    private trainingsStore: Store<fromTrainings.State>,
  ) {}

  ngOnInit(): void {
    this.trainingForm = this.createFormGroup();
    this.loading$ = this.trainingsStore.pipe(select(selectTrainingsLoading));
    this.sports$ = this.sportsStore.pipe(select(selectSportsAll));
  }

  private createFormGroup(): FormGroup {
    return this.formBuilder.group({
      name: [this.data.name, [Validators.required, Validators.minLength(2)]],
      sport_id: [this.data.sport_id, [Validators.required]],
      period_start: [this.data.period_start, [Validators.required]],
      period_end: [this.data.period_end, [Validators.required]],
      activities_per_day: [this.data.activities_per_day, [Validators.required, Validators.min(1), Validators.max(3)]],
      weekends_only: [this.data.weekends_only, [Validators.required]],
    });
  }

  private submit(): void {
    if (this.trainingForm.valid) {
      this.trainingsStore.dispatch(createTraining({data: this.trainingForm.value}));
      this.closeDialog();
    }
  }

  private closeDialog(): void {
    this.dialogRef.close(this.data);
  }
}
