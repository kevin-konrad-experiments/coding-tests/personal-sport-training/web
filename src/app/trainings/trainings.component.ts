import {ChangeDetectionStrategy, Component, HostBinding, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromTrainings from '../_core/store/trainings/trainings.reducer';
import * as fromHome from '../_core/store/home/home.reducer';
import {loadTrainings} from '../_core/store/trainings/trainings.actions';
import {Observable} from 'rxjs';
import {Training} from '../_core/models/training.model';
import {selectTrainingsAll, selectTrainingsError, selectTrainingsLoading} from '../_core/store/trainings/trainings.selectors';
import {environment} from '../../environments/environment';
import {MatDialog} from '@angular/material';
import {TrainingCreateFormComponent} from './training-create-form/training-create-form.component';
import {selectIsSmallScreen} from '../_core/store/home/home.selectors';
import {DrawerService} from '../_core/services/drawer.service';
import {TrainingDeleteFormComponent} from './training-delete-form/training-delete-form.component';

@Component({
  selector: 'app-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingsComponent implements OnInit {
  @HostBinding('class.page') pageClass = true;
  private readonly environment = environment;
  private burgerMenuVisible$: Observable<boolean>;
  private loading$: Observable<boolean>;
  private error$: Observable<any>;
  private trainings$: Observable<Training[]>;

  constructor(
    private trainingsStore: Store<fromTrainings.State>,
    private homeStore: Store<fromHome.State>,
    private matDialog: MatDialog,
    private drawerService: DrawerService,
  ) {}

  ngOnInit() {
    this.trainingsStore.dispatch(loadTrainings());

    this.burgerMenuVisible$ = this.homeStore.pipe(select(selectIsSmallScreen));
    this.loading$ = this.trainingsStore.pipe(select(selectTrainingsLoading));
    this.error$ = this.trainingsStore.pipe(select(selectTrainingsError));
    this.trainings$ = this.trainingsStore.pipe(select(selectTrainingsAll));
  }

  showCreateDialog() {
    this.matDialog.open(TrainingCreateFormComponent);
  }

  showRemoveDialog(training: Training): void {
    this.matDialog.open(TrainingDeleteFormComponent, {data: training});
  }

  async toggleDrawer(): Promise<void> {
    await this.drawerService.toggle();
  }
}
