import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrainingsRoutingModule} from './trainings-routing.module';
import {TrainingsComponent} from './trainings.component';
import {
  MatButtonModule,
  MatCardModule, MatCheckboxModule, MatDatepickerModule,
  MatDialogModule, MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListModule, MatMenuModule,
  MatProgressBarModule, MatProgressSpinnerModule, MatSelectModule,
  MatToolbarModule
} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {StoreModule} from '@ngrx/store';
import * as fromTrainings from '../_core/store/trainings/trainings.reducer';
import {EffectsModule} from '@ngrx/effects';
import {TrainingsEffects} from '../_core/store/trainings/trainings.effects';
import {HttpClientModule} from '@angular/common/http';
import {TrainingCreateFormComponent} from './training-create-form/training-create-form.component';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexModule} from '@angular/flex-layout';
import {MatDateFormats} from '@angular/material/core/typings/datetime/date-formats';

export const MY_DATE_FORMAT: MatDateFormats = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@NgModule({
  declarations: [
    TrainingsComponent,
    TrainingCreateFormComponent,
  ],
  entryComponents: [
    TrainingCreateFormComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    TrainingsRoutingModule,
    MatToolbarModule,
    TranslateModule,
    StoreModule.forFeature(fromTrainings.trainingsFeatureKey, fromTrainings.reducer),
    EffectsModule.forFeature([TrainingsEffects]),
    MatListModule,
    MatProgressBarModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMomentDateModule,
    ReactiveFormsModule,
    MatSelectModule,
    FlexModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatMenuModule,
  ]
})
export class TrainingsModule {}
