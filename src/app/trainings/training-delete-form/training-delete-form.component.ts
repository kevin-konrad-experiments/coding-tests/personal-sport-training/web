import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Store} from '@ngrx/store';
import * as fromTrainings from '../../_core/store/trainings/trainings.reducer';
import {Training} from '../../_core/models/training.model';
import {removeTraining} from '../../_core/store/trainings/trainings.actions';

@Component({
  selector: 'app-training-delete-form',
  templateUrl: './training-delete-form.component.html',
  styleUrls: ['./training-delete-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingDeleteFormComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public training: Training,
    private dialogRef: MatDialogRef<TrainingDeleteFormComponent>,
    private trainingsStore: Store<fromTrainings.State>,
  ) {}

  confirm(): void {
    this.trainingsStore.dispatch(removeTraining(this.training));
    this.dialogRef.close({deleted: true});
  }
}
