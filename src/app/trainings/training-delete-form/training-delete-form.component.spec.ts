import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingDeleteFormComponent } from './training-delete-form.component';

describe('TrainingDeleteFormComponent', () => {
  let component: TrainingDeleteFormComponent;
  let fixture: ComponentFixture<TrainingDeleteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingDeleteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingDeleteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
