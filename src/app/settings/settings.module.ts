import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import {MatButtonModule, MatIconModule, MatProgressBarModule, MatToolbarModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
  declarations: [SettingsComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule,
    TranslateModule
  ]
})
export class SettingsModule { }
