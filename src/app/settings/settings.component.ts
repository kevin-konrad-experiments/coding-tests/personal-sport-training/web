import {ChangeDetectionStrategy, Component, HostBinding, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromHome from '../_core/store/home/home.reducer';
import {DrawerService} from '../_core/services/drawer.service';
import {Observable} from 'rxjs';
import Settings from '../_core/models/settings.model';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SettingsComponent implements OnInit {
  @HostBinding('class.page') pageClass = true;
  private burgerMenuVisible$: Observable<boolean>;
  private loading$: Observable<boolean>;
  private error$: Observable<any>;
  private settings$: Observable<Settings>;

  constructor(
    private homeStore: Store<fromHome.State>,
    private drawerService: DrawerService,
  ) {}

  ngOnInit() {
  }

  async toggleDrawer(): Promise<void> {
    await this.drawerService.toggle();
  }
}
