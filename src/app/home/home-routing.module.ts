import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'trainings'
      },
      {
        path: 'trainings',
        loadChildren: () => import('../trainings/trainings.module').then(m => m.TrainingsModule)
      },
      {
        path: 'trainings/:id',
        loadChildren: () => import('../training/training.module').then(m => m.TrainingModule)
      },
      {
        path: 'sports',
        loadChildren: () => import('../sports/sports.module').then(m => m.SportsModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('../settings/settings.module').then(m => m.SettingsModule)
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
