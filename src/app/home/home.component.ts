import {AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {select, Store} from '@ngrx/store';
import * as fromSports from '../_core/store/sports/sports.reducer';
import * as fromActivities from '../_core/store/activities/activities.reducer';
import * as fromHome from '../_core/store/home/home.reducer';
import {loadSports} from '../_core/store/sports/sports.actions';
import {MediaObserver} from '@angular/flex-layout';
import {Observable} from 'rxjs';
import {HomeService} from '../_core/services/home.service';
import {selectIsSmallScreen} from '../_core/store/home/home.selectors';
import {map} from 'rxjs/operators';
import {MatDrawer} from '@angular/material';
import {DrawerService} from '../_core/services/drawer.service';
import {loadActivities} from '../_core/store/activities/activities.actions';

interface MenuItem {
  route: string;
  titleKey: string;
  icon: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('drawer', {static: true}) public drawer: MatDrawer;
  private isSmallScreen$: Observable<boolean>;
  private drawerMode$: Observable<'over' | 'push' | 'side'>;
  public readonly menuItems: MenuItem[] = [
    {
      route: '/trainings',
      titleKey: 'trainings.title',
      icon: 'timer',
    },
    {
      route: '/sports',
      titleKey: 'sports.title',
      icon: 'sports_soccer',
    },
    {
      route: '/settings',
      titleKey: 'settings.title',
      icon: 'settings',
    }
  ];

  constructor(
    private sportsStore: Store<fromSports.State>,
    private activitiesStore: Store<fromActivities.State>,
    private homeStore: Store<fromHome.State>,
    private mediaObserver: MediaObserver,
    private homeService: HomeService,
    private drawerService: DrawerService,
  ) {}

  ngOnInit(): void {
    this.homeService.init();
    this.sportsStore.dispatch(loadSports());
    this.activitiesStore.dispatch(loadActivities());
    this.isSmallScreen$ = this.homeStore.pipe(select(selectIsSmallScreen));
    this.drawerMode$ = this.isSmallScreen$.pipe(
      map((isSmallScreen: boolean) => isSmallScreen ? 'push' : 'side')
    );
  }

  ngOnDestroy(): void {
    this.homeService.destroy();
  }

  ngAfterViewInit(): void {
    this.drawerService.setDrawer(this.drawer);
  }
}
