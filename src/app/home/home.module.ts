import {NgModule} from '@angular/core';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {MatButtonModule, MatDialogModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import * as fromSports from '../_core/store/sports/sports.reducer';
import {EffectsModule} from '@ngrx/effects';
import {SportsEffects} from '../_core/store/sports/sports.effects';
import * as fromHome from '../_core/store/home/home.reducer';
import * as fromActivities from '../_core/store/activities/activities.reducer';
import { ActivitiesEffects } from '../_core/store/activities/activities.effects';
import {TrainingDeleteFormComponent} from '../trainings/training-delete-form/training-delete-form.component';

@NgModule({
  declarations: [
    HomeComponent,
    TrainingDeleteFormComponent,
  ],
  entryComponents: [
    TrainingDeleteFormComponent,
  ],
  imports: [
    HomeRoutingModule,
    MatSidenavModule,
    TranslateModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    CommonModule,
    StoreModule.forFeature(fromHome.homeFeatureKey, fromHome.reducer),
    StoreModule.forFeature(fromSports.sportsFeatureKey, fromSports.reducer),
    StoreModule.forFeature(fromActivities.activitiesFeatureKey, fromActivities.reducer),
    EffectsModule.forFeature([SportsEffects, ActivitiesEffects]),
    MatDialogModule,
  ],
})
export class HomeModule {}
