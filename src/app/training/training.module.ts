import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrainingRoutingModule} from './training-routing.module';
import {TrainingComponent} from './training.component';
import {
  MatButtonModule,
  MatCardModule, MatChipsModule, MatDialogModule, MatFormFieldModule, MatGridListModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatProgressBarModule,
  MatToolbarModule
} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {FlexModule, GridModule} from '@angular/flex-layout';
import { TrainingActivityDeleteFormComponent } from './training-activity-delete-form/training-activity-delete-form.component';
import { TrainingActivityUpdateFormComponent } from './training-activity-update-form/training-activity-update-form.component';

@NgModule({
  declarations: [
    TrainingComponent,
    TrainingActivityDeleteFormComponent,
    TrainingActivityUpdateFormComponent,
  ],
  entryComponents: [
    TrainingActivityDeleteFormComponent,
    TrainingActivityUpdateFormComponent,
  ],
  imports: [
    CommonModule,
    TrainingRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressBarModule,
    TranslateModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    FlexModule,
    MatMenuModule,
    MatDialogModule,
    MatGridListModule,
    MatChipsModule,
    GridModule,
    MatFormFieldModule,
  ]
})
export class TrainingModule {}
