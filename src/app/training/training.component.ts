import {ChangeDetectionStrategy, Component, HostBinding, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import * as fromTrainings from '../_core/store/trainings/trainings.reducer';
import * as fromHome from '../_core/store/home/home.reducer';
import {select, Store} from '@ngrx/store';
import {loadTraining} from '../_core/store/trainings/trainings.actions';
import {Observable} from 'rxjs';
import {ScheduledActivity, Training} from '../_core/models/training.model';
import {selectLoadedTraining, selectTrainingsError, selectTrainingsLoading} from '../_core/store/trainings/trainings.selectors';
import {filter, map} from 'rxjs/operators';
import {TrainingDeleteFormComponent} from '../trainings/training-delete-form/training-delete-form.component';
import {MatDialog} from '@angular/material';
import {TrainingActivityDeleteFormComponent} from './training-activity-delete-form/training-activity-delete-form.component';
import {selectIsSmallScreen} from '../_core/store/home/home.selectors';
import {TrainingActivityUpdateFormComponent} from './training-activity-update-form/training-activity-update-form.component';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingComponent implements OnInit {
  @HostBinding('class.page') pageClass = true;
  private currentDate: Date = new Date();
  private currentDateUTC: Date = new Date(Date.UTC(
    this.currentDate.getUTCFullYear(),
    this.currentDate.getUTCMonth(),
    this.currentDate.getUTCDate()
  ));
  private loading$: Observable<boolean>;
  private error$: Observable<any>;
  private training$: Observable<Training>;
  private gridCols$: Observable<number>;
  private activitiesCalendar$: Observable<{ [date: string]: ScheduledActivity[] }>;
  private dates$: Observable<{date: Date, dateStr: string}[]>;

  constructor(
    private route: ActivatedRoute,
    private trainingsStore: Store<fromTrainings.State>,
    private homeStore: Store<fromHome.State>,
    private matDialog: MatDialog,
    private router: Router,
  ) {}

  ngOnInit() {
    const id: string = this.route.snapshot.paramMap.get('id');
    this.trainingsStore.dispatch(loadTraining({id}));
    this.loading$ = this.trainingsStore.pipe(select(selectTrainingsLoading));
    this.error$ = this.trainingsStore.pipe(select(selectTrainingsError));
    this.training$ = this.trainingsStore.pipe(select(selectLoadedTraining));
    this.gridCols$ = this.homeStore.pipe(select(selectIsSmallScreen))
      .pipe(
        map(isSmallScreen => isSmallScreen ? 2 : 3)
      );
    this.activitiesCalendar$ = this.training$.pipe(
      filter(training => !!training),
      map(training => training.schedule
        .reduce((h, activity) => {
          const time = new Date(activity.date).getTime();
          return Object.assign(h, {[time]: (h[time] || []).concat(activity)});
        }, {})
      ),
    );
    this.dates$ = this.activitiesCalendar$.pipe(
      map(activitiesCalendar => Object.keys(activitiesCalendar).map(key => ({date: new Date(Number(key)), dateStr: key}))),
    );
  }

  showRemoveDialog(training: Training): void {
    this.matDialog
      .open<TrainingDeleteFormComponent, Training, {deleted: boolean}>(TrainingDeleteFormComponent, {data: training})
      .afterClosed()
      .pipe(
        filter((result) => result && !!result.deleted)
      )
      .subscribe(() => this.router.navigate(['/trainings']));
  }

  showRemoveActivityDialog(training: Training, scheduledActivity: ScheduledActivity): void {
    this.matDialog.open(TrainingActivityDeleteFormComponent, {data: {training, scheduledActivity}});
  }

  showUpdateActivityDialog(training: Training, scheduledActivity: ScheduledActivity): void {
    this.matDialog.open(TrainingActivityUpdateFormComponent, {data: {training, scheduledActivity}});
  }
}
