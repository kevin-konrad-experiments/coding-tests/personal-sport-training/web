import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ScheduledActivity, Training} from '../../_core/models/training.model';
import {select, Store} from '@ngrx/store';
import * as fromActivities from '../../_core/store/activities/activities.reducer';
import * as fromTrainings from '../../_core/store/trainings/trainings.reducer';
import {Observable} from 'rxjs';
import Activity from '../../_core/models/activity.model';
import {selectActivitiesBySport} from '../../_core/store/activities/activities.selectors';
import {updateTrainingActivity} from '../../_core/store/trainings/trainings.actions';

@Component({
  selector: 'app-training-activity-update-form',
  templateUrl: './training-activity-update-form.component.html',
  styleUrls: ['./training-activity-update-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingActivityUpdateFormComponent implements OnInit {
  private activities$: Observable<Activity[]>;
  private scheduledActivity: ScheduledActivity;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: {training: Training, scheduledActivity: ScheduledActivity},
    private dialogRef: MatDialogRef<TrainingActivityUpdateFormComponent>,
    private activitiesStore: Store<fromActivities.State>,
    private trainingsStore: Store<fromTrainings.State>,
  ) {}

  ngOnInit() {
    this.scheduledActivity = {...this.dialogData.scheduledActivity};
    this.activities$ = this.activitiesStore.pipe(select(selectActivitiesBySport, {sport: this.dialogData.training.sport}));
  }

  changeActivity(activity: Activity): void {
    this.scheduledActivity.activity = activity;
  }

  submit(): void {
    const props = {
      trainingId: this.dialogData.training.id,
      scheduledActivity: this.scheduledActivity,
    };
    this.trainingsStore.dispatch(updateTrainingActivity(props));
    this.dialogRef.close();
  }
}
