import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingActivityUpdateFormComponent } from './training-activity-update-form.component';

describe('TrainingActivityUpdateFormComponent', () => {
  let component: TrainingActivityUpdateFormComponent;
  let fixture: ComponentFixture<TrainingActivityUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingActivityUpdateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingActivityUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
