import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ScheduledActivity, Training} from '../../_core/models/training.model';
import {Store} from '@ngrx/store';
import * as fromTrainings from '../../_core/store/trainings/trainings.reducer';
import {removeTrainingActivity} from '../../_core/store/trainings/trainings.actions';

@Component({
  selector: 'app-training-activity-delete-form',
  templateUrl: './training-activity-delete-form.component.html',
  styleUrls: ['./training-activity-delete-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrainingActivityDeleteFormComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) private dialogData: {training: Training, scheduledActivity: ScheduledActivity},
    private dialogRef: MatDialogRef<TrainingActivityDeleteFormComponent>,
    private trainingsStore: Store<fromTrainings.State>,
  ) {}

  confirm(): void {
    const props = {
      trainingId: this.dialogData.training.id,
      scheduledActivityId: this.dialogData.scheduledActivity.id,
    };
    this.trainingsStore.dispatch(removeTrainingActivity(props));
    this.dialogRef.close();
  }
}
