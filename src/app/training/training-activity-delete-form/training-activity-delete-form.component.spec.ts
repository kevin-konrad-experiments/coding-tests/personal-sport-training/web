import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingActivityDeleteFormComponent } from './training-activity-delete-form.component';

describe('TrainingActivityDeleteFormComponent', () => {
  let component: TrainingActivityDeleteFormComponent;
  let fixture: ComponentFixture<TrainingActivityDeleteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrainingActivityDeleteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingActivityDeleteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
