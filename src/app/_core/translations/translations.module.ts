import {APP_INITIALIZER, ModuleWithProviders, NgModule} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslationsService} from './translations.service';
import {defaultTranslationsModuleConfig, TRANSLATIONS_CONFIG, TranslationsConfig} from './translations.config';
import {environment} from '../../../environments/environment';
import {TranslateModuleConfig} from '@ngx-translate/core/public_api';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function initTranslations(translationsService: TranslationsService) {
  return () => translationsService.init();
}

const translateModuleConfig: TranslateModuleConfig = {
  useDefaultLang: true,
  loader: {
    provide: TranslateLoader,
    useFactory: createTranslateLoader,
    deps: [HttpClient]
  },
};

@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forRoot(translateModuleConfig)
  ],
  exports: [TranslateModule],
})
export class TranslationsModule {

  static forRoot(translationsModuleConfig?: TranslationsConfig): ModuleWithProviders {
    return {
      ngModule: TranslationsModule,
      providers: [
        {provide: APP_INITIALIZER, useFactory: initTranslations, deps: [TranslationsService], multi: true},
        {
          provide: TRANSLATIONS_CONFIG,
          useValue: {
            ...defaultTranslationsModuleConfig,
            ...environment.translations,
            ...translationsModuleConfig
          }
        },
      ]
    };
  }
}
