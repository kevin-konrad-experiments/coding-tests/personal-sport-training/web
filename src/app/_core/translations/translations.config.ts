import {InjectionToken} from '@angular/core';

export interface TranslationsConfig {
  defaultLanguage: string;
  languages: string[];
}

export const TRANSLATIONS_CONFIG = new InjectionToken<TranslationsConfig>('TranslationsConfig');

export const defaultTranslationsModuleConfig: TranslationsConfig = {
  defaultLanguage: 'en',
  languages: ['en'],
};
