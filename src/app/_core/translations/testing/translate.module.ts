import {NgModule} from '@angular/core';
import {TranslatePipeMock} from './translate.pipe';
import {TranslateLoader, TranslateModule, TranslatePipe, TranslateService} from '@ngx-translate/core';
import {TranslateServiceMock} from './translate.service';
import {Observable, of} from 'rxjs';

const translations: any = {};

class FakeLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of(translations);
  }
}

@NgModule({
  declarations: [
    TranslatePipeMock
  ],
  providers: [
    {provide: TranslateService, useClass: TranslateServiceMock},
    {provide: TranslatePipe, useClass: TranslatePipeMock},
  ],
  imports: [
    TranslateModule.forRoot({
      loader: {provide: TranslateLoader, useClass: FakeLoader},
    })
  ],
  exports: [
    TranslatePipeMock,
    TranslateModule
  ]
})
export class TranslateTestingModule {}
