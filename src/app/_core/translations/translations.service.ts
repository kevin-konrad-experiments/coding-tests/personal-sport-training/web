import {Inject, Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {registerLocaleData} from '@angular/common';
import {TRANSLATIONS_CONFIG, TranslationsConfig} from './translations.config';

@Injectable({
  providedIn: 'root'
})
export class TranslationsService {

  constructor(
    @Inject(TRANSLATIONS_CONFIG) private config: TranslationsConfig,
    private translate: TranslateService,
  ) {}

  init(): void {
    this.initLanguages();
  }

  private initLanguages(): void {
    const navigatorLanguage: string = window.navigator.language.split('-')[0];
    const currentLanguage: string = this.config.languages.includes(navigatorLanguage)
      ? navigatorLanguage
      : this.config.defaultLanguage;

    this.translate.addLangs(this.config.languages);
    this.translate.setDefaultLang(this.config.defaultLanguage);
    this.translate.getLangs().forEach(this.loadLocaleData);
    this.translate.use(currentLanguage);
  }

  private loadLocaleData = (lang: string) => {
    import(`@angular/common/locales/${lang}.js`).then(locale => registerLocaleData(locale.default));
  }
}
