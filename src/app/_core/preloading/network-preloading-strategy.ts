import {Injectable} from '@angular/core';
import {PreloadingStrategy, Route} from '@angular/router';
import {EMPTY, Observable} from 'rxjs';

export declare var navigator;

@Injectable({
  providedIn: 'root'
})
export class NetworkPreloadingStrategy implements PreloadingStrategy {

  preload(route: Route, load: () => Observable<any>): Observable<any> {
    return this.hasFastConnection() ? load() : EMPTY;
  }

  hasFastConnection(): boolean {
    const connection = navigator.connection;

    if (!connection) {
      return true;
    }

    if (connection.saveData) {
      return false;
    }

    const slowConnections = ['slow-2g', '2g'];
    const effectiveType = connection.effectiveType || '';
    return !slowConnections.includes(effectiveType);
  }
}
