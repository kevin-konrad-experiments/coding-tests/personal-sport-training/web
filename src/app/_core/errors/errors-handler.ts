import {ErrorHandler, Injectable, NgZone} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class ErrorsHandler implements ErrorHandler {

  constructor(
    private zone: NgZone,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
  ) {}

  handleError(error: Error | HttpErrorResponse): void {
    const message = this.getErrorText(error);
    console.error(error);
    this.showNotification(message);
  }

  showNotification(message: string, action?: string): void {
    this.zone.run(() => {
      this.snackBar.open(message, action, {
        duration: 3000,
        verticalPosition: 'bottom',
        horizontalPosition: 'center',
      });
    });
  }

  private getErrorText(error: Error | HttpErrorResponse | any): string {
    if (error.promise && error.rejection) {
      error = error.rejection;
    }

    if (error instanceof HttpErrorResponse) {
      return this.translate.instant(`errors.http.${error.status}`);
    } else {
      return error.message;
    }
  }
}
