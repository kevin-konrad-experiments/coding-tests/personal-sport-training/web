import {ErrorHandler, NgModule} from '@angular/core';
import {ErrorsHandler} from './errors-handler';
import {MatSnackBarModule} from '@angular/material';

@NgModule({
  imports: [MatSnackBarModule],
  providers: [
    {provide: ErrorHandler, useClass: ErrorsHandler},
  ]
})
export class ErrorsModule {}
