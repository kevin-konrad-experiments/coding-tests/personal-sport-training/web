import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NetworkPreloadingStrategy} from '../preloading';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('../../home/home.module').then(m => m.HomeModule),
  },
  {
    path: '**',
    redirectTo: '',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: NetworkPreloadingStrategy})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
