import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ScheduledActivity, Training, TrainingCreateBody} from '../models/training.model';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TrainingsService {
  private readonly url: string = `${environment.api.baseUrl}/trainings`;

  constructor(private http: HttpClient) {}

  public getAll(): Observable<Training[]> {
    return this.http.get<Training[]>(this.url);
  }

  public getById(id: string): Observable<Training> {
    return this.http.get<Training>(`${this.url}/${id}`);
  }

  public create(data: TrainingCreateBody): Observable<Training> {
    return this.http.post<Training>(this.url, data);
  }

  public remove(id: string): Observable<Training[]> {
    return this.http.delete<Training[]>(`${this.url}/${id}`);
  }

  public removeActivity(trainingId: string, activityId: string): Observable<ScheduledActivity[]> {
    return this.http.delete<ScheduledActivity[]>(`${this.url}/${trainingId}/activities/${activityId}`);
  }

  public updateActivity(trainingId: string, scheduledActivity: ScheduledActivity): Observable<ScheduledActivity> {
    return this.http.put<ScheduledActivity>(`${this.url}/${trainingId}/activities/${scheduledActivity.id}`, scheduledActivity);
  }
}
