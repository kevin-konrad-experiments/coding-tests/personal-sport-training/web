import { Injectable } from '@angular/core';
import {MediaObserver} from '@angular/flex-layout';
import {filter, map, withLatestFrom} from 'rxjs/operators';
import * as fromHome from '../store/home/home.reducer';
import {select, Store} from '@ngrx/store';
import {changeScreenSize} from '../store/home/home.actions';
import {Observable, Subscription} from 'rxjs';
import {NavigationEnd, Router} from '@angular/router';
import {selectIsSmallScreen} from '../store/home/home.selectors';
import {DrawerService} from './drawer.service';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private readonly subscription: Subscription = new Subscription();
  private isSmallScreen$: Observable<boolean>;

  constructor(
    private store: Store<fromHome.State>,
    private mediaObserver: MediaObserver,
    private router: Router,
    private drawerService: DrawerService,
  ) {}

  init(): void {
    this.isSmallScreen$ = this.store.pipe(select(selectIsSmallScreen));

    this.subscription.add(this.initSmallScreenSubscription());
    this.subscription.add(this.initRouterEventsSubscription());
  }

  destroy(): void {
    this.subscription.unsubscribe();
  }

  initSmallScreenSubscription(): Subscription {
    return this.mediaObserver.asObservable()
      .pipe(
        map((mediaChange) => !!mediaChange.find(change => change.mqAlias === 'sm' || change.mqAlias === 'xs')),
      )
      .subscribe((isSmallScreen) => this.store.dispatch(changeScreenSize({isSmallScreen})));
  }

  initRouterEventsSubscription(): Subscription {
    return this.router.events
      .pipe(
        withLatestFrom(this.isSmallScreen$),
        filter(([event, isSmallScreen]) => isSmallScreen && event instanceof NavigationEnd)
      )
      .subscribe(() => this.drawerService.close());
  }
}
