import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import Sport from '../models/sport.model';

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {
  private readonly url: string = `${environment.api.baseUrl}/activities`;

  constructor(private http: HttpClient) {}

  public getAll(): Observable<Sport[]> {
    return this.http.get<Sport[]>(this.url);
  }
}
