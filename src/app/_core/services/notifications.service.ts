import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(
    private snackBar: MatSnackBar,
    private translate: TranslateService,
  ) {}

  public showToast(messageKey: string, messageParams: object, actionKey: string = 'app.close') {
    const message: string = this.translate.instant(messageKey, messageParams);
    const action: string = this.translate.instant(actionKey);

    this.snackBar.open(message, action, {duration: 3000});
  }
}
