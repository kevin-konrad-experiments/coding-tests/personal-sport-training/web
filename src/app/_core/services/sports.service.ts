import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import Sport, {SportCreateBody, SportUpdateBody} from '../models/sport.model';

@Injectable({
  providedIn: 'root'
})
export class SportsService {
  private readonly url: string = `${environment.api.baseUrl}/sports`;

  constructor(private http: HttpClient) {}

  public getAll(): Observable<Sport[]> {
    return this.http.get<Sport[]>(this.url);
  }

  public create(sport: SportCreateBody): Observable<Sport> {
    const formData = new FormData();
    formData.append('name', sport.name);
    formData.append('activities_ids', JSON.stringify(sport.activities_ids));
    formData.append('image', sport.image);

    return this.http.post<Sport>(this.url, formData);
  }

  public update(sport: SportUpdateBody): Observable<Sport> {
    const formData = new FormData();
    formData.append('name', sport.name);
    formData.append('activities_ids', JSON.stringify(sport.activities_ids));
    formData.append('image', sport.image);

    return this.http.put<Sport>(`${this.url}/${sport.id}`, formData);
  }

  public remove(id: string): Observable<Sport[]> {
    return this.http.delete<Sport[]>(`${this.url}/${id}`);
  }
}
