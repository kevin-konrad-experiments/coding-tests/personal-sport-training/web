import {Injectable} from '@angular/core';
import {MatDrawer, MatDrawerToggleResult} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class DrawerService {

  private drawer: MatDrawer;

  public setDrawer(drawer: MatDrawer) {
    this.drawer = drawer;
  }

  public open() {
    return this.drawer.open();
  }

  public close() {
    return this.drawer.close();
  }

  public async toggle(): Promise<MatDrawerToggleResult> {
    return await this.drawer.toggle();
  }
}
