import Sport from './sport.model';
import Activity from './activity.model';

export enum TrainingMomentOfDay {
  MORNING,
  AFTERNOON,
  EVENING,
}

export interface TrainingCreateBody {
  sport_id: string;
  name: string;
  activities_per_day: number;
  weekends_only: boolean;
  period_start: Date;
  period_end: Date;
}

export interface TrainingPeriod {
  start: Date;
  end: Date;
}

export interface ScheduledActivity {
  id: string;
  date: Date;
  dateStr: string;
  moment: TrainingMomentOfDay;
  activity: Activity;
}

export interface Training {
  id: string;
  name: string;
  sport: Sport;
  period: TrainingPeriod;
  activities_per_day: number;
  weekends_only: boolean;
  schedule: ScheduledActivity[];
}
