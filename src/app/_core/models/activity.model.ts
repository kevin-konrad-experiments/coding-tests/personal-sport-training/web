export default interface Activity {
  id: string;
  name: string;
}
