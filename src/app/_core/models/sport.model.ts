import Activity from './activity.model';

export default interface Sport {
  id: string;
  name: string;
  image_url: string;
  activities: Activity[];
}

export interface SportCreateBody {
  name: string;
  image: File;
  activities_ids: string[];
}

export interface SportUpdateBody {
  id: string;
  name: string;
  image?: File;
  activities_ids: string[];
}
