import { createAction, props } from '@ngrx/store';
import {ScheduledActivity, Training, TrainingCreateBody} from '../../models/training.model';

// Load all trainings
export const loadTrainings = createAction(
  '[Trainings] Load Trainings'
);

export const loadTrainingsSuccess = createAction(
  '[Trainings] Load Trainings Success',
  props<{ data: Training[] }>()
);

export const loadTrainingsFailure = createAction(
  '[Trainings] Load Trainings Failure',
  props<{ error: any }>()
);

// Load a single training
export const loadTraining = createAction(
  '[Trainings] Load Training',
  props<{ id: string }>()
);

export const loadTrainingSuccess = createAction(
  '[Trainings] Load Training Success',
  props<{ data: Training }>()
);

export const loadTrainingFailure = createAction(
  '[Trainings] Load Training Failure',
  props<{ error: any }>()
);

// Create a new training
export const createTraining = createAction(
  '[Trainings] Create Training',
  props<{ data: TrainingCreateBody }>()
);

export const createTrainingSuccess = createAction(
  '[Trainings] Create Training Success',
  props<{ data: Training }>()
);

export const createTrainingFailure = createAction(
  '[Trainings] Create Training Failure',
  props<{ error: any }>()
);

// Remove
export const removeTraining = createAction(
  '[Trainings] Remove Training',
  props<{id: string}>()
);

export const removeTrainingSuccess = createAction(
  '[Trainings] Remove Training Success',
  props<{data: Training[]}>()
);

export const removeTrainingFailure = createAction(
  '[Trainings] Remove Training Failure',
  props<{error: any}>()
);

// Remove activity
export const removeTrainingActivity = createAction(
  '[Trainings] Remove Training Activity',
  props<{trainingId: string, scheduledActivityId: string}>()
);

export const removeTrainingActivitySuccess = createAction(
  '[Trainings] Remove Training Activity Success',
  props<{data: ScheduledActivity[]}>()
);

export const removeTrainingActivityFailure = createAction(
  '[Trainings] Remove Training Activity Failure',
  props<{error: any}>()
);

// Update activity
export const updateTrainingActivity = createAction(
  '[Trainings] Update Training Activity',
  props<{trainingId: string, scheduledActivity: ScheduledActivity}>()
);

export const updateTrainingActivitySuccess = createAction(
  '[Trainings] Update Training Activity Success',
  props<{data: ScheduledActivity}>()
);

export const updateTrainingActivityFailure = createAction(
  '[Trainings] Update Training Activity Failure',
  props<{error: any}>()
);
