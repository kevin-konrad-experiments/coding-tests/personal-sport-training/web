import {Action, createReducer, on} from '@ngrx/store';
import * as TrainingsActions from './trainings.actions';
import {Training} from '../../models/training.model';

export const trainingsFeatureKey = 'trainings';

export interface State {
  loading: boolean;
  error: any;
  loadedTraining: Training;
  trainings: Training[];
}

export const initialState: State = {
  loading: false,
  error: undefined,
  loadedTraining: undefined,
  trainings: [],
};

const trainingsReducer = createReducer(
  initialState,

  // Load all
  on(TrainingsActions.loadTrainings, state => ({...state,
    loading: true,
  })),
  on(TrainingsActions.loadTrainingsSuccess, (state, action) => ({...state,
    loading: false,
    trainings: action.data,
  })),
  on(TrainingsActions.loadTrainingsFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),

  // Load single
  on(TrainingsActions.loadTraining, state => ({...state,
    loading: true,
  })),
  on(TrainingsActions.loadTrainingSuccess, (state, action) => ({...state,
    loading: false,
    loadedTraining: action.data,
  })),
  on(TrainingsActions.loadTrainingFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),

  // Create
  on(TrainingsActions.createTraining, state => ({...state,
    loading: true,
  })),
  on(TrainingsActions.createTrainingSuccess, (state, action) => ({...state,
    loading: false,
    trainings: [...state.trainings, action.data],
  })),
  on(TrainingsActions.createTrainingFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),

  // Remove
  on(TrainingsActions.removeTraining, state => ({...state,
    loading: true,
  })),
  on(TrainingsActions.removeTrainingSuccess, (state, action) => ({...state,
    loading: false,
    trainings: state.trainings.filter(training => !action.data.find(element => element.id === training.id)),
  })),
  on(TrainingsActions.removeTrainingFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),

  // Remove activity
  on(TrainingsActions.removeTrainingActivity, state => ({...state,
    loading: true,
  })),
  on(TrainingsActions.removeTrainingActivitySuccess, (state, action) => ({...state,
    loading: false,
    loadedTraining: ({...state.loadedTraining,
      schedule: state.loadedTraining.schedule
        .filter(scheduledActivity => !action.data.find(element => element.id === scheduledActivity.id))
    }),
  })),
  on(TrainingsActions.removeTrainingActivityFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),

  // Update activity
  on(TrainingsActions.updateTrainingActivity, state => ({...state,
    loading: true,
  })),
  on(TrainingsActions.updateTrainingActivitySuccess, (state, action) => ({...state,
    loading: false,
    loadedTraining: ({...state.loadedTraining,
      schedule: state.loadedTraining.schedule.map(scheduledActivity => {
        return scheduledActivity.id === action.data.id
          ? {...action.data}
          : scheduledActivity;
      })
    }),
  })),
  on(TrainingsActions.updateTrainingActivityFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return trainingsReducer(state, action);
}
