import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, concatMap, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import * as TrainingsActions from './trainings.actions';
import {TrainingsService} from '../../services/trainings.service';
import {Router} from '@angular/router';
import {NotificationsService} from '../../services/notifications.service';

@Injectable()
export class TrainingsEffects {

  constructor(
    private actions$: Actions,
    private trainingsService: TrainingsService,
    private notificationsService: NotificationsService,
    private router: Router,
  ) {}

  loadTrainings$ = createEffect(() => this.actions$.pipe(
    ofType(TrainingsActions.loadTrainings),
    concatMap(() => this.trainingsService.getAll().pipe(
      map(trainings => trainings.map(training => ({...training,
        period: {
          start: new Date(training.period.start),
          end: new Date(training.period.end)
        }
      }))),
      map(data => TrainingsActions.loadTrainingsSuccess({data})),
      catchError(error => of(TrainingsActions.loadTrainingsFailure({error})))
    ))
  ));

  loadTraining$ = createEffect(() => this.actions$.pipe(
    ofType(TrainingsActions.loadTraining),
    concatMap(action => this.trainingsService.getById(action.id).pipe(
      map(training => ({...training,
        period: {
          start: new Date(training.period.start),
          end: new Date(training.period.end)
        }
      })),
      map(data => TrainingsActions.loadTrainingSuccess({data})),
      catchError(error => of(TrainingsActions.loadTrainingFailure({error})))
    ))
  ));

  createTraining$ = createEffect(() => this.actions$.pipe(
    ofType(TrainingsActions.createTraining),
    concatMap(action => this.trainingsService.create(action.data).pipe(
      tap(training => this.router.navigate(['/trainings', training.id])),
      map(training => ({...training,
        period: {
          start: new Date(training.period.start),
          end: new Date(training.period.end)
        }
      })),
      map(training => TrainingsActions.createTrainingSuccess({data: training})),
      tap(success => this.notificationsService.showToast('trainings.created', {name: success.data.name})),
      catchError(error => of(TrainingsActions.createTrainingFailure({error})))
    ))
  ));

  removeTraining$ = createEffect(() => this.actions$.pipe(
    ofType(TrainingsActions.removeTraining),
    concatMap(action => this.trainingsService.remove(action.id).pipe(
      map(data => TrainingsActions.removeTrainingSuccess({data})),
      tap(success => this.notificationsService.showToast('trainings.removed', {name: success.data[0].name})),
      catchError(error => of(TrainingsActions.removeTrainingFailure({error})))
    ))
  ));

  removeTrainingActivity$ = createEffect(() => this.actions$.pipe(
    ofType(TrainingsActions.removeTrainingActivity),
    concatMap(action => this.trainingsService.removeActivity(action.trainingId, action.scheduledActivityId).pipe(
      map(data => TrainingsActions.removeTrainingActivitySuccess({data})),
      catchError(error => of(TrainingsActions.removeTrainingActivityFailure({error})))
    ))
  ));

  updateTrainingActivity$ = createEffect(() => this.actions$.pipe(
    ofType(TrainingsActions.updateTrainingActivity),
    concatMap(action => this.trainingsService.updateActivity(action.trainingId, action.scheduledActivity).pipe(
      map(data => TrainingsActions.updateTrainingActivitySuccess({data})),
      catchError(error => of(TrainingsActions.updateTrainingActivityFailure({error})))
    ))
  ));
}
