import * as fromTrainings from './trainings.reducer';
import { selectTrainingsState } from './trainings.selectors';

describe('Trainings Selectors', () => {
  it('should select the feature state', () => {
    const result = selectTrainingsState({
      [fromTrainings.trainingsFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
