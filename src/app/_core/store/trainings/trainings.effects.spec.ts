import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { TrainingsEffects } from './trainings.effects';

describe('TrainingsEffects', () => {
  let actions$: Observable<any>;
  let effects: TrainingsEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TrainingsEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<TrainingsEffects>(TrainingsEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
