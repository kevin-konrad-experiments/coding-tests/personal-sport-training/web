import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromTrainings from './trainings.reducer';
import {Training} from '../../models/training.model';

export const selectTrainingsState = createFeatureSelector<fromTrainings.State>(
  fromTrainings.trainingsFeatureKey,
);

export const selectTrainingsLoading = createSelector<fromTrainings.State, fromTrainings.State, boolean>(
  selectTrainingsState,
  (state: fromTrainings.State) => state.loading,
);

export const selectTrainingsError = createSelector<fromTrainings.State, fromTrainings.State, any>(
  selectTrainingsState,
  (state: fromTrainings.State) => state.error,
);

export const selectTrainingsAll = createSelector<fromTrainings.State, fromTrainings.State, Training[]>(
  selectTrainingsState,
  (state: fromTrainings.State) => state.trainings,
);

export const selectLoadedTraining = createSelector<fromTrainings.State, fromTrainings.State, Training>(
  selectTrainingsState,
  (state: fromTrainings.State) => state.loadedTraining,
);
