import {Action, createReducer, on} from '@ngrx/store';
import * as SportsActions from './sports.actions';
import Sport from '../../models/sport.model';

export const sportsFeatureKey = 'sports';

export interface State {
  loading: boolean;
  error: any;
  sports: Sport[];
}

export const initialState: State = {
  loading: false,
  error: undefined,
  sports: [],
};

const sportsReducer = createReducer(
  initialState,

  // Load all
  on(SportsActions.loadSports, state => ({...state,
    loading: true,
  })),
  on(SportsActions.loadSportsSuccess, (state, action) => ({...state,
    loading: false,
    sports: action.data,
  })),
  on(SportsActions.loadSportsFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),

  // Create
  on(SportsActions.createSport, state => ({...state,
    loading: true,
  })),
  on(SportsActions.createSportSuccess, (state, action) => ({...state,
    loading: false,
    sports: [...state.sports, action.data],
  })),
  on(SportsActions.createSportFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),

  // Update
  on(SportsActions.updateSport, state => ({...state,
    loading: true,
  })),
  on(SportsActions.updateSportSuccess, (state, action) => ({...state,
    loading: false,
    sports: state.sports.map(sport => ({...sport,
      name: sport.id === action.data.id ? action.data.name : sport.name,
      image_url: sport.id === action.data.id ? action.data.image_url : sport.image_url,
      activities: sport.id === action.data.id ? action.data.activities : sport.activities,
    })),
  })),
  on(SportsActions.updateSportFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),

  // Remove
  on(SportsActions.removeSport, state => ({...state,
    loading: true,
  })),
  on(SportsActions.removeSportSuccess, (state, action) => ({...state,
    loading: false,
    sports: state.sports.filter(sport => !action.data.find(element => element.id === sport.id)),
  })),
  on(SportsActions.removeSportFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return sportsReducer(state, action);
}
