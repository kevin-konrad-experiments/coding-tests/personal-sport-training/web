import * as fromSports from './sports.reducer';
import { selectSportsState } from './sports.selectors';

describe('Sports Selectors', () => {
  it('should select the feature state', () => {
    const result = selectSportsState({
      [fromSports.sportsFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
