import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, concatMap, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import * as SportsActions from './sports.actions';
import {SportsService} from '../../services/sports.service';
import {NotificationsService} from '../../services/notifications.service';

@Injectable()
export class SportsEffects {

  constructor(
    private actions$: Actions,
    private sportsService: SportsService,
    private notificationsService: NotificationsService,
  ) {}

  loadSports$ = createEffect(() => this.actions$.pipe(
    ofType(SportsActions.loadSports),
    concatMap(() => this.sportsService.getAll().pipe(
      map(data => SportsActions.loadSportsSuccess({data})),
      catchError(error => of(SportsActions.loadSportsFailure({error})))
    ))
  ));

  createSport$ = createEffect(() => this.actions$.pipe(
    ofType(SportsActions.createSport),
    concatMap(action => this.sportsService.create(action.data).pipe(
      map(data => SportsActions.createSportSuccess({data})),
      tap(success => this.notificationsService.showToast('sports.created', {name: success.data.name})),
      catchError(error => of(SportsActions.createSportFailure({error})))
    ))
  ));

  updateSport$ = createEffect(() => this.actions$.pipe(
    ofType(SportsActions.updateSport),
    concatMap(action => this.sportsService.update(action.data).pipe(
      map(data => SportsActions.updateSportSuccess({data})),
      tap(success => this.notificationsService.showToast('sports.updated', {name: success.data.name})),
      catchError(error => of(SportsActions.updateSportFailure({error})))
    ))
  ));

  removeSport$ = createEffect(() => this.actions$.pipe(
    ofType(SportsActions.removeSport),
    concatMap(action => this.sportsService.remove(action.id).pipe(
      map(data => SportsActions.removeSportSuccess({data})),
      tap(success => this.notificationsService.showToast('sports.removed', {name: success.data[0].name})),
      catchError(error => of(SportsActions.removeSportFailure({error})))
    ))
  ));
}
