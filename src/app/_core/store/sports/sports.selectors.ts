import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromSports from './sports.reducer';

export const selectSportsState = createFeatureSelector<fromSports.State>(
  fromSports.sportsFeatureKey
);

export const selectSportsLoading = createSelector(
  selectSportsState,
  (state: fromSports.State) => state.loading,
);

export const selectSportsError = createSelector(
  selectSportsState,
  (state: fromSports.State) => state.error,
);

export const selectSportsAll = createSelector(
  selectSportsState,
  (state: fromSports.State) => state.sports,
);
