import {createAction, props} from '@ngrx/store';
import Sport, {SportCreateBody, SportUpdateBody} from '../../models/sport.model';

// Load all
export const loadSports = createAction(
  '[Sports] Load Sports'
);

export const loadSportsSuccess = createAction(
  '[Sports] Load Sports Success',
  props<{ data: Sport[] }>()
);

export const loadSportsFailure = createAction(
  '[Sports] Load Sports Failure',
  props<{ error: any }>()
);

// Create
export const createSport = createAction(
  '[Sports] Create Sport',
  props<{data: SportCreateBody}>()
);

export const createSportSuccess = createAction(
  '[Sports] Create Sport Success',
  props<{data: Sport}>()
);

export const createSportFailure = createAction(
  '[Sports] Create Sport Failure',
  props<{error: any}>()
);

// Update
export const updateSport = createAction(
  '[Sports] Update Sport',
  props<{data: SportUpdateBody}>()
);

export const updateSportSuccess = createAction(
  '[Sports] Update Sport Success',
  props<{data: Sport}>()
);

export const updateSportFailure = createAction(
  '[Sports] Update Sport Failure',
  props<{error: any}>()
);

// Remove
export const removeSport = createAction(
  '[Sports] Remove Sport',
  props<{id: string}>()
);

export const removeSportSuccess = createAction(
  '[Sports] Remove Sport Success',
  props<{data: Sport[]}>()
);

export const removeSportFailure = createAction(
  '[Sports] Remove Sport Failure',
  props<{error: any}>()
);
