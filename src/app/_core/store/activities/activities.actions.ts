import { createAction, props } from '@ngrx/store';
import Activity from '../../models/activity.model';

export const loadActivities = createAction(
  '[Activities] Load Activities'
);

export const loadActivitiesSuccess = createAction(
  '[Activities] Load Activities Success',
  props<{ data: Activity[] }>()
);

export const loadActivitiesFailure = createAction(
  '[Activities] Load Activities Failure',
  props<{ error: any }>()
);
