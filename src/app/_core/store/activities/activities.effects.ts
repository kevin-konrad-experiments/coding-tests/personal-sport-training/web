import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, map, concatMap} from 'rxjs/operators';
import {of} from 'rxjs';
import * as ActivitiesActions from './activities.actions';
import {ActivitiesService} from '../../services/activities.service';

@Injectable()
export class ActivitiesEffects {

  constructor(
    private actions$: Actions,
    private activitiesService: ActivitiesService,
  ) {
  }

  loadActivities$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ActivitiesActions.loadActivities),
      concatMap(() => this.activitiesService.getAll().pipe(
        map(data => ActivitiesActions.loadActivitiesSuccess({data})),
        catchError(error => of(ActivitiesActions.loadActivitiesFailure({error})))
      ))
    );
  });
}
