import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromActivities from './activities.reducer';
import Activity from '../../models/activity.model';
import Sport from '../../models/sport.model';

export const selectActivitiesState = createFeatureSelector<fromActivities.State>(
  fromActivities.activitiesFeatureKey
);

export const selectActivitiesAll = createSelector(
  selectActivitiesState,
  (state: fromActivities.State) => state.activities,
);

export const selectActivitiesBySport = createSelector(
  selectActivitiesAll,
  (activities: Activity[], props: {sport: Sport}) => activities
    .filter(activity => props.sport.activities
      .find(element => element.id === activity.id)
    ),
);
