import { Action, createReducer, on } from '@ngrx/store';
import * as ActivitiesActions from './activities.actions';
import Activity from '../../models/activity.model';

export const activitiesFeatureKey = 'activities';

export interface State {
  loading: boolean;
  error: any;
  activities: Activity[];
}

export const initialState: State = {
  loading: false,
  error: undefined,
  activities: [],
};

const activitiesReducer = createReducer(
  initialState,

  // Load all
  on(ActivitiesActions.loadActivities, state => ({...state,
    loading: true,
  })),
  on(ActivitiesActions.loadActivitiesSuccess, (state, action) => ({...state,
    loading: false,
    activities: action.data,
  })),
  on(ActivitiesActions.loadActivitiesFailure, (state, action) => ({...state,
    loading: false,
    error: action.error,
  })),
);

export function reducer(state: State | undefined, action: Action) {
  return activitiesReducer(state, action);
}
