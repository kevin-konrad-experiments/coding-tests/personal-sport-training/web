import * as fromActivities from './activities.reducer';
import { selectActivitiesState } from './activities.selectors';

describe('Activities Selectors', () => {
  it('should select the feature state', () => {
    const result = selectActivitiesState({
      [fromActivities.activitiesFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
