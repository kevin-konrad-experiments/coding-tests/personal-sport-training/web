import {storeFreeze} from 'ngrx-store-freeze';
import * as fromRouter from '@ngrx/router-store';
import {environment} from '../../../../environments/environment';
import {ActionReducerMap, MetaReducer} from '@ngrx/store';

export interface State {
  router: fromRouter.RouterReducerState;
}

export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [storeFreeze]
  : [];
