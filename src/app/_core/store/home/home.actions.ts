import { createAction, props } from '@ngrx/store';

export const changeScreenSize = createAction(
  '[Home] Change screen size',
  props<{isSmallScreen: boolean}>()
);




