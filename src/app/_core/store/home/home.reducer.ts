import { Action, createReducer, on } from '@ngrx/store';
import * as HomeActions from './home.actions';

export const homeFeatureKey = 'home';

export interface State {
  isSmallScreen: boolean;
}

export const initialState: State = {
  isSmallScreen: false,
};

const homeReducer = createReducer(
  initialState,

  on(HomeActions.changeScreenSize, (state, action) => ({...state,
    isSmallScreen: action.isSmallScreen
  })),

);

export function reducer(state: State | undefined, action: Action) {
  return homeReducer(state, action);
}
