import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import Sport, {SportUpdateBody} from '../../_core/models/sport.model';
import {select, Store} from '@ngrx/store';
import * as fromSports from '../../_core/store/sports/sports.reducer';
import * as fromActivities from '../../_core/store/activities/activities.reducer';
import {updateSport} from '../../_core/store/sports/sports.actions';
import Activity from '../../_core/models/activity.model';
import {Observable} from 'rxjs';
import {selectActivitiesAll} from '../../_core/store/activities/activities.selectors';
import {FileValidator} from 'ngx-material-file-input';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-sport-edit-form',
  templateUrl: './sport-update-form.component.html',
  styleUrls: ['./sport-update-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SportUpdateFormComponent implements OnInit {
  public environment = environment;
  private sportForm: FormGroup;
  private activities$: Observable<Activity[]>;
  private selectedActivitiesIds: string[];
  private readonly megaBytesMaxSize = 5;
  private readonly imageMaxSize = this.megaBytesMaxSize * 2 ** 20;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: Sport,
    private dialogRef: MatDialogRef<SportUpdateFormComponent>,
    private formBuilder: FormBuilder,
    private sportsStore: Store<fromSports.State>,
    private activitiesStore: Store<fromActivities.State>,
  ) {}

  ngOnInit(): void {
    this.selectedActivitiesIds = this.dialogData.activities.map(activity => activity.id);
    this.activities$ = this.activitiesStore.pipe(select(selectActivitiesAll));
    this.sportForm = this.createFormGroup();
  }

  private createFormGroup(): FormGroup {
    return this.formBuilder.group({
      name: [this.dialogData.name, [Validators.required, Validators.minLength(2)]],
      image: [undefined, [FileValidator.maxContentSize(this.imageMaxSize)]],
    });
  }

  private addActivity(activity: Activity): void {
    this.selectedActivitiesIds.push(activity.id);
  }

  private removeActivity(activity: Activity): void {
    const index = this.selectedActivitiesIds.findIndex(id => activity.id === id);

    if (index >= 0) {
      this.selectedActivitiesIds.splice(index, 1);
    }
  }

  private getFormattedData(): SportUpdateBody {
    return {
      id: this.dialogData.id,
      name: this.sportForm.value.name,
      image: this.sportForm.value.image ? this.sportForm.value.image.files[0] : undefined,
      activities_ids: this.selectedActivitiesIds,
    };
  }

  private submit(): void {
    if (this.sportForm.valid) {
      const data = this.getFormattedData();
      this.sportsStore.dispatch(updateSport({data}));
      this.dialogRef.close();
    }
  }
}
