import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SportUpdateFormComponent} from './sport-update-form.component';

describe('SportUpdateFormComponent', () => {
  let component: SportUpdateFormComponent;
  let fixture: ComponentFixture<SportUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SportUpdateFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
