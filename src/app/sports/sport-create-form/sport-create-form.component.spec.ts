import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SportCreateFormComponent } from './sport-create-form.component';

describe('SportCreateFormComponent', () => {
  let component: SportCreateFormComponent;
  let fixture: ComponentFixture<SportCreateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SportCreateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportCreateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
