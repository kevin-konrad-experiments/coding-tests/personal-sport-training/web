import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import * as fromSports from '../../_core/store/sports/sports.reducer';
import {createSport} from '../../_core/store/sports/sports.actions';
import Activity from '../../_core/models/activity.model';
import {SportCreateBody} from '../../_core/models/sport.model';
import {Observable} from 'rxjs';
import * as fromActivities from '../../_core/store/activities/activities.reducer';
import {selectActivitiesAll} from '../../_core/store/activities/activities.selectors';
import {FileValidator} from 'ngx-material-file-input';

@Component({
  selector: 'app-sport-create-form',
  templateUrl: './sport-create-form.component.html',
  styleUrls: ['./sport-create-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SportCreateFormComponent implements OnInit {
  private sportForm: FormGroup;
  private activities$: Observable<Activity[]>;
  private selectedActivitiesIds: string[];
  private readonly megaBytesMaxSize = 5;
  private readonly imageMaxSize = this.megaBytesMaxSize * 2 ** 20;

  constructor(
    private dialogRef: MatDialogRef<SportCreateFormComponent>,
    private formBuilder: FormBuilder,
    private sportsStore: Store<fromSports.State>,
    private activitiesStore: Store<fromActivities.State>,
  ) { }

  ngOnInit(): void {
    this.selectedActivitiesIds = [];
    this.activities$ = this.activitiesStore.pipe(select(selectActivitiesAll));
    this.sportForm = this.createFormGroup();
  }

  private createFormGroup(): FormGroup {
    return this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      image: [undefined, [Validators.required, FileValidator.maxContentSize(this.imageMaxSize)]],
    });
  }

  private addActivity(activity: Activity): void {
    this.selectedActivitiesIds.push(activity.id);
  }

  private removeActivity(activity: Activity): void {
    const index = this.selectedActivitiesIds.findIndex(id => activity.id === id);

    if (index >= 0) {
      this.selectedActivitiesIds.splice(index, 1);
    }
  }

  private getFormattedData(): SportCreateBody {
    return {
      name: this.sportForm.value.name,
      image: this.sportForm.value.image.files[0],
      activities_ids: this.selectedActivitiesIds,
    };
  }

  private submit(): void {
    if (this.sportForm.valid) {
      const data = this.getFormattedData();
      this.sportsStore.dispatch(createSport({data}));
      this.dialogRef.close();
    }
  }
}
