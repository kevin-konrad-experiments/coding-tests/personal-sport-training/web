import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import Sport from '../../_core/models/sport.model';
import {Store} from '@ngrx/store';
import * as fromSports from '../../_core/store/sports/sports.reducer';
import {removeSport} from '../../_core/store/sports/sports.actions';

@Component({
  selector: 'app-sport-delete-form',
  templateUrl: './sport-delete-form.component.html',
  styleUrls: ['./sport-delete-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SportDeleteFormComponent {

  constructor(
    @Inject(MAT_DIALOG_DATA) public sport: Sport,
    private dialogRef: MatDialogRef<SportDeleteFormComponent>,
    private sportsStore: Store<fromSports.State>,
  ) {}

  confirm(): void {
    this.sportsStore.dispatch(removeSport(this.sport));
    this.dialogRef.close();
  }
}
