import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SportDeleteFormComponent } from './sport-delete-form.component';

describe('SportDeleteFormComponent', () => {
  let component: SportDeleteFormComponent;
  let fixture: ComponentFixture<SportDeleteFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SportDeleteFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportDeleteFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
