import {ChangeDetectionStrategy, Component, HostBinding, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import Sport from '../_core/models/sport.model';
import {select, Store} from '@ngrx/store';
import * as fromSports from '../_core/store/sports/sports.reducer';
import * as fromHome from '../_core/store/home/home.reducer';
import {DrawerService} from '../_core/services/drawer.service';
import {selectIsSmallScreen} from '../_core/store/home/home.selectors';
import {selectSportsAll, selectSportsError, selectSportsLoading} from '../_core/store/sports/sports.selectors';
import {loadSports} from '../_core/store/sports/sports.actions';
import {MatDialog} from '@angular/material';
import {SportUpdateFormComponent} from './sport-update-form/sport-update-form.component';
import {SportCreateFormComponent} from './sport-create-form/sport-create-form.component';
import {SportDeleteFormComponent} from './sport-delete-form/sport-delete-form.component';

@Component({
  selector: 'app-sports',
  templateUrl: './sports.component.html',
  styleUrls: ['./sports.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SportsComponent implements OnInit {
  @HostBinding('class.page') pageClass = true;
  private readonly environment = environment;
  private burgerMenuVisible$: Observable<boolean>;
  private loading$: Observable<boolean>;
  private error$: Observable<any>;
  private sports$: Observable<Sport[]>;

  constructor(
    private homeStore: Store<fromHome.State>,
    private drawerService: DrawerService,
    private sportsStore: Store<fromSports.State>,
    private matDialog: MatDialog,
  ) {}

  ngOnInit() {
    this.sportsStore.dispatch(loadSports());

    this.burgerMenuVisible$ = this.homeStore.pipe(select(selectIsSmallScreen));
    this.loading$ = this.sportsStore.select(selectSportsLoading);
    this.error$ = this.sportsStore.select(selectSportsError);
    this.sports$ = this.sportsStore.select(selectSportsAll);
  }

  async toggleDrawer(): Promise<void> {
    await this.drawerService.toggle();
  }

  showRemoveDialog(sport: Sport): void {
    this.matDialog.open(SportDeleteFormComponent, {data: sport});
  }

  showUpdateDialog(sport: Sport): void {
    this.matDialog.open(SportUpdateFormComponent, {data: sport});
  }

  showCreateDialog(): void {
    this.matDialog.open(SportCreateFormComponent);
  }
}
