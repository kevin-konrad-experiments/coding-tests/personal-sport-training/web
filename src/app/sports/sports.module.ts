import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SportsRoutingModule} from './sports-routing.module';
import {SportsComponent} from './sports.component';
import {TranslateModule} from '@ngx-translate/core';
import {
  MatButtonModule,
  MatCardModule, MatChipsModule, MatDialogModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListModule, MatMenuModule,
  MatProgressBarModule,
  MatToolbarModule
} from '@angular/material';
import {SportUpdateFormComponent} from './sport-update-form/sport-update-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SportCreateFormComponent} from './sport-create-form/sport-create-form.component';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {FlexModule} from '@angular/flex-layout';
import { ImagePreviewComponent } from './image-preview/image-preview.component';
import { SportDeleteFormComponent } from './sport-delete-form/sport-delete-form.component';

@NgModule({
  declarations: [
    SportsComponent,
    ImagePreviewComponent,
    SportCreateFormComponent,
    SportUpdateFormComponent,
    SportDeleteFormComponent,
  ],
  entryComponents: [
    SportCreateFormComponent,
    SportUpdateFormComponent,
    SportDeleteFormComponent,
  ],
  imports: [
    CommonModule,
    SportsRoutingModule,
    TranslateModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule,
    MatListModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatChipsModule,
    MatDialogModule,
    MaterialFileInputModule,
    FlexModule,
    MatMenuModule,
  ],
})
export class SportsModule {}
