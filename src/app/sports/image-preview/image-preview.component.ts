import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImagePreviewComponent implements OnChanges {
  @Input() private readonly image: File;
  @Input() private sourceUrl: string;
  private source: string;

  constructor(private cdRef: ChangeDetectorRef) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.image) {
      this.updateSource();
    } else {
      this.source = this.sourceUrl || undefined;
    }
  }

  updateSource(): void {
    const reader = new FileReader();

    reader.onload = (e: any) => {
      this.source = e.target.result;
      this.cdRef.markForCheck();
    };

    reader.readAsDataURL(this.image);
  }
}
