export const environment = {
  production: true,
  api: {
    baseUrl: 'http://localhost:3000/api/v1'
  },
  translations: {
    defaultLanguage: 'en',
    languages: ['en']
  }
};
