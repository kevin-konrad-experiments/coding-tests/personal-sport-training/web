export const environment = {
  production: false,
  api: {
    baseUrl: 'http://localhost:3000/api/v1',
    imagesUrl: 'http://localhost:3000/static'
  },
  translations: {
    defaultLanguage: 'en',
    languages: ['en']
  }
};
